# Testboxed iOS
Testboxed Xcode Project for iOS and iPadOS.

# Why we created this
None of our team has a Mac, so you'll have to compile Testboxed yourself for iOS & iPadOS.

# How to compile
We haven't worked with Xcode, so please figure it out for yourself. You can help to complete README via [pull requests](https://codeberg.org/DFCTeam/Testboxed-iOS/pulls).